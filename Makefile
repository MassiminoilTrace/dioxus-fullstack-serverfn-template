debug:
	dx build --features web
	cargo run --features ssr

release:
	dx build --features web --release
	cargo run --features ssr --release